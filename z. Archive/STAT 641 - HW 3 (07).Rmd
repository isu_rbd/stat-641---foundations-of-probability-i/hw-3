---
title: "STAT 641 - HW 3"
author: "Ricardo Batista"
header-includes:
  - \usepackage{bbm}
date: "9/23/2019"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Problem 1.3

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{\mathcal{F}_6 \text{ is a } \sigma \text{algebra}}$

&nbsp;&nbsp;&nbsp;&nbsp; Let $A \subset \Omega$ be countable, then $A \in \mathcal{F}_6$. If $A^c$ is also countable, $A^c \in \mathcal{F}_6$. If, instead, $A^c$ is uncountable, $(A^c)^c = A$ is also in $\mathcal{F}_6$.  

&nbsp;&nbsp;&nbsp;&nbsp; Specifically, note $\emptyset \subset \Omega$ and $\vert \emptyset \vert = 0$, so $\emptyset \in \mathcal{F}_6$. Similarly, since $\Omega \subset \Omega$ and $\Omega^c = \emptyset$ which is countable, $\Omega \in \mathcal{F}_6$.  

&nbsp;&nbsp;&nbsp;&nbsp; Also, let $\{A_n\}_{n \geq 1} \subset \mathcal{F}_6$ and suppose $A = \bigcup_{n \geq 1} A_n$ is countable, then $A, A^c \in \mathcal{F}_6$. Now suppose $A$ is uncountable. This means some $A_n$ are uncountable. Then we have that $A^c = \left( \bigcup_{n \geq 1} A_n\right)^c = \bigcap_{n \geq 1} A_n^c$ is countable since, for the uncountable $A_n$, $A_n^c$ is countable. Therefore,  $A, A^c \in \mathcal{F}_6$.  


&nbsp;&nbsp;&nbsp;&nbsp; $\underline{\mathcal{F}_6 \text{ is a } \sigma \text{algebra}}$  

&nbsp;&nbsp;&nbsp;&nbsp; Let $A \in \mathcal{F}_6$. Then, since $A$ is either countable or uncountable (by definition of $\mathcal{F}_6$), $\mu(A) \in \{0, 1\}$ and  $\{0, 1\} \subset [0, \infty]$. 

&nbsp;&nbsp;&nbsp;&nbsp; More specifically, note that, since $\emptyset$ is countable, $\mu(\emptyset) = 0$.

&nbsp;&nbsp;&nbsp;&nbsp; Finally, let $\{A_n\}_{n \geq 1} \subset \mathcal{F}_6$ be a disjoint collection of sets with $A = \bigcup_{n \geq 1}A_n \in \mathcal{F}_6$. Define $\mathcal{U} = \{A_n \subset A: \vert A_n \vert = \infty\}$. That is, $\mathcal{U}$ is the collection of uncountable $A_n$. Then, 

$$
\mu \left( \bigcup_{n \geq 1}A_n \right)
= \sum_{n = 1}^\infty \mathbbm{1}_\mathcal{U}(A_n) 
= \sum_{n = 1}^\infty \mu(A_n).
$$


# Problem 1.8

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{\sigma \langle \mathcal{A} \rangle \subset \mathcal{P}(\Omega)}$

&nbsp;&nbsp;&nbsp;&nbsp; True by definition of power set.  

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{\mathcal{P}(\Omega) \subset \sigma \langle \mathcal{A} \rangle}$

&nbsp;&nbsp;&nbsp;&nbsp; Let $B \in \mathcal{P}(\Omega)$. If $B = \{n : n \in \mathbb{N}, n \geq i\}$, $i\in \mathbb{N}$, then $B = A_i \in \sigma \langle \mathcal{A} \rangle$. If instead $B = \{n : n \in \mathbb{N}, n \leq i\}$, $i \in \mathbb{N}$, then $B = A_{i + 1}^c \in \sigma \langle \mathcal{A} \rangle$. If $B = \{ n : n \in \mathbb{N}, i \leq n \leq j\}$, $i,j \in \mathbb{N}$, then $B = A_i \cap A_{j + 1}^c \in \sigma \langle \mathcal{A} \rangle$. If $B$ is none of the types above, then $B$ can be expressed as a countable union of sets of the "interval" type just described. In other words, $B = \bigcup_{n \geq 1} B_n$ where  $B_n = \{ m : m \in \mathbb{N}, i \leq m \leq j\}$, $i,j \in \mathbb{N}$. Since $B_n = A_i \cap A_{j + 1}^c \in \sigma \langle \mathcal{A} \rangle$ and $\bigcup_{n \geq 1} B_n \in \sigma \langle \mathcal{A} \rangle$ by definition of sigma algebras.  

\pagebreak

# Problem 1.10

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{ \mathcal{O}_2 \subset \sigma \langle \mathcal{O}_1 \rangle}$

&nbsp;&nbsp;&nbsp;&nbsp; Recall that $\mathcal{O}_1 = \{(a_1, b_1) \times (a_2, b_2) : -\infty \leq a_i < b_i \leq \infty, i = 1, 2\}$ and consider the collection of sets in $\mathcal{O}_1$ such that $a_1 = a_2 = -\infty$

$$
\{(-\infty, b_1) \times (-\infty, b_2): -\infty \leq b_i \leq \infty, i = 1, 2\}. 
$$

Realize this collection is in $\mathcal{O}_2$, so $\mathcal{O}_2 \subset \mathcal{O}_1 \subset \sigma \langle \mathcal{O}_1 \rangle$.

\vspace{10pt}

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{ \mathcal{O}_3 \subset \sigma \langle \mathcal{O}_1 \rangle}$

&nbsp;&nbsp;&nbsp;&nbsp; Since $\mathbb{Q} \subset \mathbb{R}$, $\mathcal{O}_3 \subset \mathcal{O}_1 \subset \sigma\langle \mathcal{O}_1 \rangle$.

\vspace{10pt}

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{ \mathcal{O}_4 \subset \sigma \langle \mathcal{O}_1 \rangle}$

&nbsp;&nbsp;&nbsp;&nbsp; Since $\mathbb{Q} \subset \mathbb{R}$, $\mathcal{O}_4 \subset \mathcal{O}_2 \subset \mathcal{O}_1 \subset \sigma\langle \mathcal{O}_1 \rangle$.

\vspace{10pt}

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{ \mathcal{O}_1 \subset \sigma \langle \mathcal{O}_2 \rangle}$

&nbsp;&nbsp;&nbsp;&nbsp; Let $A = (a_1, b_1) \times (a_2, b_2) \in \mathcal{O}_1$. Note that sets $(-\infty, b_1) \times(-\infty, b_2)$ and $(-\infty, a_1) \times(-\infty, a_2)$ belong in $\sigma \langle \mathcal{O}_2 \rangle$, as do complements and intersections. Specifically,

$$
((-\infty, b_1) \times (-\infty, b_2)) \cap ((-\infty, a_1) \times (-\infty, a_2))^c
= [a_1, b_1) \times [a_2, b_2)
$$

belongs in $\sigma \langle \mathcal{O}_2 \rangle$. Similarly, 

$$
A_n =((-\infty, b_1) \times (-\infty, b_2)) 
\cap 
\left(\left(-\infty, a_1 + \tfrac{1}{n}\right) \times \left(-\infty, a_2 + \tfrac{1}{n}\right)\right)^c
= \left[a_1 + \tfrac{1}{n}, b_1\right) \times \left[a_2 + \tfrac{1}{n}, b_2\right)
$$

and, the infinitely countable union

$$
\bigcup_{n \geq 1} A_n 
= \bigcup_{n \geq 1} \left(\left[a_1 + \tfrac{1}{n}, b_1\right) \times \left[a_2 + \tfrac{1}{n}, b_2\right)\right) = (a_1, b_1) \times (a_2, b_2) = A \in \sigma \langle \mathcal{O}_2 \rangle.
$$

since $\sigma \langle \mathcal{O}_2 \rangle$ is a $\sigma$-algebra. Therefore, $A \in \mathcal{O}_1 \implies A \in \sigma \langle \mathcal{O}_2 \rangle$, i.e., $\mathcal{O}_1 \subset \sigma \langle \mathcal{O}_2 \rangle$.  

\vspace{10pt}

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{ \mathcal{O}_3 \subset \sigma \langle \mathcal{O}_2 \rangle}$

&nbsp;&nbsp;&nbsp;&nbsp; Since $\mathbb{Q} \subset \mathbb{R}$, $\mathcal{O}_3 \subset \mathcal{O}_1$ so $\mathcal{O}_3 \subset \sigma \langle \mathcal{O}_2 \rangle$ by arguments above.

\vspace{10pt}

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{ \mathcal{O}_4 \subset \sigma \langle \mathcal{O}_2 \rangle}$

&nbsp;&nbsp;&nbsp;&nbsp; Since $\mathbb{Q} \subset \mathbb{R}$, $\mathcal{O}_4 \subset \mathcal{O}_2 \subset \sigma \langle \mathcal{O}_2 \rangle$

\vspace{10pt}

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{ \mathcal{O}_1 \subset \sigma \langle \mathcal{O}_3 \rangle}$

&nbsp;&nbsp;&nbsp;&nbsp; Let $A = (a_1, b_1) \times (a_2, b_2) \in \mathcal{O}_1$. For each $n \in \mathbb{N}$ let 

$$
I_n = \left[a_1,  a_1 + \frac{1}{n}\right).
$$

By the density of the rational numbers, each of these nonempty intervals $I_n$ contain a rational number. For each $n \in \mathbb{N}$ let $r_n \in I_n$ be any of the rational numbers in this interval. Then we claim that $\{r_n\}_{n\geq 1} \subset \mathbb{Q}$ converges to $a_1$.  

&nbsp;&nbsp;&nbsp;&nbsp; To show this, let $\varepsilon > 0$ be given. Then shoose $N_\varepsilon$ such that $\frac{1}{N_\varepsilon} < \varepsilon$. Then if $n \geq N_\varepsilon$ we have $\frac{1}{n} \leq \frac{1}{N_\varepsilon} < \varepsilon$ and so:

$$
\vert r_n - a_1 \vert < \frac{1}{n} \leq \frac{1}{N_\varepsilon} < \varepsilon
$$

Therefore $\{r_n\}_{n\geq 1}$ converges to $a_1$ and by construction $\{r_n\}_{n\geq 1} \subset \mathbb{Q}$.  

&nbsp;&nbsp;&nbsp;&nbsp; Now suppose we build similar sequences of rational numbers $\{m_n\}$, $\{p_n\}$, and $\{q_n\}$ that converge to $b_1$, $a_2$, and $b_2$, respectively. (Note that for $b_i$, $i = 1, 2$ we would define $I_n = [b_i - \tfrac{1}{n}, b_i)$.) Then,

$$
\bigcup_{n \geq 1} \left(\left(r_n, m_n\right) \times \left(p_n, q_n\right)\right) = (a_1, b_1) \times (a_2, b_2) = A \in \sigma \langle \mathcal{O}_3 \rangle.
$$

so, $\mathcal{ \mathcal{O}_1 \subset \sigma \langle \mathcal{O}_3 \rangle}$.  

\vspace{10pt}

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{ \mathcal{O}_2 \subset \sigma \langle \mathcal{O}_3 \rangle}$

&nbsp;&nbsp;&nbsp;&nbsp; By our previous work above, $\mathcal{O}_2 \subset \mathcal{O}_1 \subset \sigma \langle \mathcal{O}_3 \rangle$.   

\vspace{10pt}

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{ \mathcal{O}_4 \subset \sigma \langle \mathcal{O}_3 \rangle}$

&nbsp;&nbsp;&nbsp;&nbsp; Same proof as for $\mathcal{O}_2 \subset \sigma \langle \mathcal{O}_1 \rangle$.  


\vspace{10pt}

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{ \mathcal{O}_2 \subset \sigma \langle \mathcal{O}_4 \rangle}$

&nbsp;&nbsp;&nbsp;&nbsp; Let $A = (-\infty, x_1) \times (-\infty, x_1) \in \mathcal{O}_2$. We proceed  using a similar proof as in $\mathcal{O}_1 \subset \sigma \langle \mathcal{O}_3\rangle$. That is, let $x_1 \in \mathbb{R}$ and

$$
I_n = [x_1 - \tfrac{1}{n}, x_1).
$$

By the density of the rationals, each $I_n$ constains a rational number. Let $r_n \in I_n$ be any of the rational numbers in this interval, then $\{r_n\}_{n \geq 1} \subset \mathbb{Q}$ converges to $x_1$. Assuming we build $\{q_n\}_{n \geq 1} \subset \mathbb{Q}$ which converges to $x_2 \in \mathbb{R}$, we have that

$$
\bigcup_{n \geq 1} \left(\left(-\infty, r_n\right) \times \left(-\infty, q_n\right)\right) = (-\infty, x_1) \times (-\infty, x_2) = A \in \sigma \langle \mathcal{O}_4 \rangle.
$$

so $\mathcal{O}_2 \subset \sigma \langle \mathcal{O}_4 \rangle$.

\vspace{10pt}

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{ \mathcal{O}_1 \subset \sigma \langle \mathcal{O}_4 \rangle}$

&nbsp;&nbsp;&nbsp;&nbsp; Let $A = (a_1, b_1) \times (a_2, b_2) \in \mathcal{O}_1$. We just showed that $\mathcal{O}_2 \subset \sigma \langle \mathcal{O}_4 \rangle$. Therefore, $\mathcal{O}_1 \subset \sigma \langle \mathcal{O}_4 \rangle$ by the proof for $\mathcal{O}_1 \subset \sigma \langle \mathcal{O}_2 \rangle$ since infinitely countable union of elements in $\mathcal{O}_2$ are also in $\sigma \langle \mathcal{O}_4 \rangle$.




\vspace{10pt}

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{ \mathcal{O}_3 \subset \sigma \langle \mathcal{O}_4 \rangle}$

&nbsp;&nbsp;&nbsp;&nbsp; Same proof as for $\mathcal{O}_1 \subset \sigma \langle \mathcal{O}_2 \rangle$.  


\pagebreak


# Problem 1.18

&nbsp;&nbsp;&nbsp;&nbsp; Let $A \equiv \bigcap_{n \geq 1} A_n$. First, note that 

$$
A^c = \left[ \bigcap_{n \geq 1} A_n \right]^c = \bigcup_{n \geq 1} A_n^c = \mathbb{N}
$$

so $A = \emptyset$ and $\mu(A) = \mu(\emptyset) = 0$. Now, for $\lim_{n \rightarrow \infty} \mu(A_n)$ to equal zero, it must be true that for given $\varepsilon > 0$,

$$
n \geq N_\varepsilon \hspace{15pt} \implies \hspace{15pt} \mu(A_n) < \varepsilon.
$$

which is clearly not true since $\mu(A_n) = \infty$ for all $n$.  


# Question 1.19

&nbsp;&nbsp;&nbsp;&nbsp; **Preliminaries**: The following proof makes substantial use of predicate logic (i.e., logic symbols). More specifically, it employs "$\overset{\text{B.1}}{\implies}$" to mean "implied by condition B.1." These are the foregoing conditions:

- *Semialgebra*:
  - S.1: $D, E \in \mathcal{C} \implies D \cap E \in \mathcal{C}$
  - S.2: for any $D \in \mathcal{C}$ there exists $\{E_i\}_{i = 1}^k \subset \mathcal{C}$ such that $E_i \cap E_j = \emptyset$ for $i \neq j$ and $D^c = \bigcup_{i = 1}^k E_i$

- $\mathcal{A}(C)$
  - A.1: $\mathcal{A}(\mathcal{C})$ includes all finite unions of sets in $\mathcal{C}$

## (a)

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{\mathcal{A} \langle \mathcal{C} \rangle \subset \mathcal{A}(\mathcal{C})}$

&nbsp;&nbsp;&nbsp;&nbsp; Let $D \in \mathcal{C}$. Then,

$$
\begin{aligned}
D \in \mathcal{C} &\overset{\text{S.2}}{\implies} (\exists E_i)(D \cap E_i = \emptyset) 
\hspace{3pt} \overset{\text{S.1}}{\implies} \hspace{3pt} D \cap E_i = \emptyset \in \mathcal{C}
\hspace{3pt} \overset{\text{A.1}}{\implies} \hspace{3pt} \emptyset \in \mathcal{A}(\mathcal{C})\\[10pt]
D \in \mathcal{C} &\overset{\text{A.1}}{\implies} D \in \mathcal{A}(\mathcal{C})\\[10pt]
D \in \mathcal{C} &\overset{\text{S.2}}{\implies}
    \left(\exists \{E_i\}_{i = 1}^k \subset \mathcal{C}\right)
    \left(D^c = \bigcup_{i = 1}^k E_i\right)
    \hspace{3pt} \overset{\text{A.1}}{\implies} \hspace{3pt} D^c \in \mathcal{A}(\mathcal{C})\\[10pt]
    D \in \mathcal{C} &\overset{\text{S.2}}{\implies}
    \left(\exists \{E_i\}_{i = 1}^k \subset \mathcal{C}\right)
    \left(D^c = \bigcup_{i = 1}^k E_i\right)
    \hspace{3pt} \overset{\text{A.1}}{\implies} \hspace{3pt} D \cup D^c = \Omega \in \mathcal{A}(\mathcal{C})\\[10pt]
\end{aligned}
$$
Finally, note that for $\{A_i\}_{i = 1}^n \in \mathcal{A}(\mathcal{C})$,

$$
A_i = \bigcup_{j = 1}^{k_i} B_{i,j} \hspace{15pt} \text{where } B_{i,j} \in \mathcal{C}.
$$

Therefore,

$$
\bigcup_{i = 1}^{n} A_i = \bigcup_{i = 1}^{n} \bigcup_{j = 1}^{k_i} B_{i,j}
$$

which is just some finite union of sets in $\mathcal{C}$, so $\bigcup_{i = 1}^{n} A_i \in \mathcal{A}(\mathcal{C})$. We have just shown that $\mathcal{A}(\mathcal{C})$ is a $\sigma$-algebra and since $\mathcal{A} \langle \mathcal{C} \rangle$ is the smallest, $\mathcal{A} \langle \mathcal{C} \rangle \subset \mathcal{A}(\mathcal{C})$.

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{\mathcal{A}(\mathcal{C}) \subset \mathcal{A} \langle \mathcal{C} \rangle}$

&nbsp;&nbsp;&nbsp;&nbsp; Let $A \in \mathcal{A}(\mathcal{C})$. Then possibly $A = B_i \in \mathcal{C}$ since $\mathcal{A}(\mathcal{C})$ contains all finite unions in $\mathcal{C}$. Alternatively, we could have $A = \bigcup_{i = 1}^k B_i$ for $k > 1$ where $B_i \in \mathcal{C}$. Then, since $\mathcal{C}$ is a semialgebra, there exist $\{E_{ij}\}_{j = 1}^{n_i} \in \mathcal{C}$ for each $B_i$ such that $B_i^c = \bigcup_{j = 1}^{n_i}E_{ij}$. Now since $\mathcal{C}$ is closed under finite intersections and $\mathcal{A}(\mathcal{C})$ contains all finite unions in $\mathcal{C}$, 

$$
\bigcup_{j = 1}^{n_i} \bigcap_{i = 1}^kE_{ij} 
= \bigcap_{i = 1}^k \bigcup_{j = 1}^{n_i}E_{ij}
= \bigcap_{i = 1}^kB_i^c 
=\left(\bigcup_{i = 1}^kB_i\right)^c
= A^c \in \mathcal{A}(\mathcal{C})
$$

## (b)

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{\sigma \langle \mathcal{C} \rangle \subset \sigma \langle \mathcal{A}(\mathcal{C}) \rangle}$

&nbsp;&nbsp;&nbsp;&nbsp; Since $\mathcal{C} \subset \mathcal{A}(\mathcal{C})$, then $\sigma \langle \mathcal{C} \rangle \subset \sigma \langle \mathcal{A}(\mathcal{C}) \rangle$. 

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{\sigma \langle \mathcal{A}(\mathcal{C}) \rangle \subset \sigma \langle \mathcal{C} \rangle}$

&nbsp;&nbsp;&nbsp;&nbsp; Let $A \in \mathcal{C}$. Note that the only difference between an algebra and a semialgebra is that an algebra is closed under set difference whereas a semialgebra is semi closed under set difference. Now consider that since $\mathcal{C}$ is a semialgebra, there exists $\{B_i\}_{i = 1}^k \subset \mathcal{C}$ with $B_i \cap B_j = \emptyset$ for $i \neq j$ and $A^c = \bigcup_{i = 1}^k B_i$. Then, since $\mathcal{A}(\mathcal{C})$ is an algebra, $A^c \in \mathcal{A}(\mathcal{C})$. Although $A^c$ is not necessarily in $\mathcal{C}$, $A^c \in \sigma \langle \mathcal{C} \rangle$ since $\sigma$-algebras inlcudes all countable unions of sets in $\sigma \langle \mathcal{C} \rangle$. Therefore, $\mathcal{A}(\mathcal{C}) \subset \sigma \langle \mathcal{C} \rangle$ and any countable union created from elements in $\mathcal{A}(\mathcal{C})$, i.e., any element in $\sigma \langle\mathcal{A}(\mathcal{C}) \rangle$, is in $\sigma \langle \mathcal{C} \rangle$.  

# Problem 1.20

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{(3.4)}$

&nbsp;&nbsp;&nbsp;&nbsp; Let $A = \emptyset$. Recall that $\emptyset \in \mathcal{C}$. Then setting $\{A_n\}_{n \geq 1} = \emptyset$ yields

$$
\mu^*(\emptyset) = \inf \left\{ \sum_{n = 1}^\infty \mu(A_n) : \{A_n\}_{n \geq 1} \subset \mathcal{C}, 
A \subset \bigcup_{n \geq 1} A_n\right\} = \mu(\emptyset) = 0
$$

which is the infimum since measures are non-negative.  

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{(3.5)}$

&nbsp;&nbsp;&nbsp;&nbsp; Suppose

$$
\{G_n\}_{n \geq 1} = \arg \min \left\{ \sum_{n = 1}^\infty \mu(E_n) : \{E_n\}_{n \geq 1} \subset \mathcal{C}, 
A \subset \bigcup_{n \geq 1} E_n\right\}
$$

That is, $G := \bigcup_{n \geq 1} G_n \supset A$ is the smallest possible cover of $A$. Since $A \subset B$, then either $G$ is also the smallest cover of $B$ or $G \not\supset B$. Suppose $G \not\supset B$ and let 

$$
\{H_n\}_{n \geq 1} = \arg \min \left\{ \sum_{n = 1}^\infty \mu(E_n) : \{E_n\}_{n \geq 1} \subset \mathcal{C}, 
B \subset \bigcup_{n \geq 1} E_n\right\}
$$

That is, $H := \bigcup_{n \geq 1} H_n \supset B$ is the smallest possible cover of $B$. Note that $G \subset H$. Since $\mathcal{C}$ is a semialgebra, we have that $G_n \in \mathcal{C}$ implies there exists $\{C_i\}_{i = 1}^k \subset \mathcal{C}$ such that $C_i \cap C_j = \emptyset$ for $i \neq j$ and $G_n^c = \bigcup_{i = 1}^k C_i$. Also since $\mathcal{C}$ is a semialgebra, $C_i \cap H_n \in \mathcal{C}$. Therefore, 

$$
H = \bigcup_{i \geq 1} H_i = G \cup \left(\bigcup_{i \geq 1} \bigcup_{j \geq 1} H_i \cap C_j\right)
$$

and so

$$
\begin{aligned}
\sum_{i = 1}^\infty G_n &\leq \sum_{i = 1}^\infty G_n + \sum_{i = 1}^\infty \sum_{j = 1}^\infty H_i \cap C_j\\[10pt]
\mu^*(A) &\leq \mu^*(B).
\end{aligned}
$$


&nbsp;&nbsp;&nbsp;&nbsp; $\underline{(3.6)}$

&nbsp;&nbsp;&nbsp;&nbsp;

[Use finite subadditivity and an argument similar to above to prove for each n]
[How to prove for infty??]

# Problem 1.24

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{A, B \in c \implies A \cap B \in \mathcal{C}_2}$

&nbsp;&nbsp;&nbsp;&nbsp; Let $A = I_1 \times I_2$ and $B = I_3 \times I_4$ where each $I_j$ equals either

- $(a, b]$ where $-\infty \leq  a \leq b < \infty$ or 
- $(a, \infty)$ where $-\infty \leq a \infty$.

Note that if $A \cap B = \emptyset \in \mathcal{C}_2$. Otherwise, note

$$
A \cap B = (I_1 \times I_2) \cap (I_3 \times I_4) = (I_1 \cap I_3) \times (I_2 \cap I_4).
$$

Now realize that $I_i \cap I_j$ falls into these three cases

- $(a, b] \cap (c, d]$ equals $(c, b]$ or $(a, d]$
- $(a, b] \cap (c,\infty)$ equals $(c, d]$
- $(a, \infty) \cap (c, \infty)$ equals $(a, \infty)$ or $(c, \infty)$

all of which are sets in $\mathcal{C}$, so $A \cap B \in \mathcal{C}_2$.  

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{A \in \mathcal{C}_2 \implies \left(\exists \{B_i\}_{i = 1}^k \subset \mathcal{C}_2\right)\left(B_i \cap B_j = \emptyset \text{ for } i \neq j\right)\left(A^c = \bigcup_{i = 1}^k B_i\right)}$

&nbsp;&nbsp;&nbsp;&nbsp; Let $A$ defined as above. Note that for each $I_j$, $I_j^c = \mathbb{R} \setminus I_j = E_{j1} \cup E_{j2}$ where $E{ji} \in \mathcal{C}$ and $E_{j1} \cup E_{j1} = \emptyset$. (Keep in mind that at most both equal the emptyset.) Then we have

$$
A^c = (I_1 \times I_2)^c = (E_{11} \times (-\infty, \infty)) \cup (E_{12} \times (-\infty, \infty)) \cup (I_1 \times E_{21}) \cup (I_1 \times E_{22}) = B_1 \cup B_2 \cup B_3 \cup B_4
$$

where $B_i \cup B_j = \emptyset$ for $i \neq j$. Therefore, for every $A \in \mathcal{C}_2$ we can build $ \{B_i\}_{i = 1}^4 \subset \mathcal{C}_2$ as above, which is what we set to prove.  

$$
\begin{aligned}
B_1 &= (-\infty, x_{11}] \times (y_{11}, y_{12}]\\[10pt]
B_2 &= (x_{11}, x_{12}] \times (y_{12}, \infty)\\[10pt]
B_3 &= (x_{12}, \infty) \times (y_{11}, y_{12}]\\[10pt]
B_4 &= (x_{11}, x_{12}] \times (y_{11}, -\infty) 
\end{aligned}
$$

and note $\{B_i\} \subset \mathcal{C}_2$ and $\bigcup_{i = 1}^4 B_i = A^c$.  

# Last problem

## (i)

&nbsp;&nbsp;&nbsp;&nbsp; Note that for all $x, y \in (0, \infty)$ we have that

$$
\begin{aligned}
0 < x \leq y\\[10pt]
0 < F(x) &\leq F(y) \leq 1
\end{aligned}
$$
so F is non-decreasing. 

&nbsp;&nbsp;&nbsp;&nbsp; Now recall that $F$ is right continuous if $\lim_{\delta \rightarrow 0} F(x + \delta) = F(x)$, so we seek to show that for every $\varepsilon > 0$ there is a $\delta_\varepsilon > 0$ such that

$$
y - x \leq \delta_\varepsilon \hspace{15pt} \implies \hspace{15pt} \vert F(y) - F(x) \vert < \varepsilon
$$

Let $\varepsilon > 0$ and $\delta > 0$. First note that Then we have that, since $F$ is non-decreasing,

$$
\begin{aligned}
F(x + \delta) - F(x)
&= \left[(x + \delta) \mathbbm{1} \{0 < x + \delta < 1\} + \mathbbm{1} \{x + \delta \geq 1\}\right]
- \left[x\mathbbm{1} \{0 < x < 1\} + \mathbbm{1} \{x \geq 1\}\right]
\end{aligned}
$$

Then if $x \geq 1$, then

$$
\begin{aligned}
F(x + \delta) - F(x) &=
\left[\mathbbm{1} \{x + \delta \geq 1\}\right] - \left[\mathbbm{1} \{x \geq 1\}\right] = 1 - 1 = 0
\end{aligned}
$$

so any $\delta$ will work. Else, if $x < 1$ and $x + \delta > 1$,

$$
\begin{aligned}
F(x + \delta) - F(x) &=
\left[\mathbbm{1} \{x + \delta \geq 1\}\right] - \left[x\mathbbm{1} \{0 < x < 1\}\right] = 1 - x.
\end{aligned}
$$

Note that setting $\delta_\varepsilon = 1 - x$ only works when $\varepsilon > 1 - x$. Finally, if both $x < 1$ and $x + \delta < 1$, 

$$
\begin{aligned}
F(x + \delta) - F(x)
&= \left[(x + \delta) \mathbbm{1} \{0 < x + \delta < 1\}\right]
- \left[x\mathbbm{1} \{0 < x < 1\}\right] = x + \delta - x = \delta.
\end{aligned}
$$

Note that if $x < 1$, we can select $\delta$ such that $x < x + \delta_\varepsilon < 1$. So setting $\delta_\varepsilon < \varepsilon$ 

$$
y - x < \delta_\varepsilon \implies F(x + \delta) - F(x) = \vert  F(x + \delta) - F(x) \vert = \delta_\varepsilon< \varepsilon
$$

where $F(x + \delta) - F(x) = \vert  F(x + \delta) - F(x) \vert$ since $F$ is nondecreasing.

## (ii)

&nbsp;&nbsp;&nbsp;&nbsp; First note that the definition of $F$ (i.e., $F(x)$ is defined on $\mathbb{R}$) implies that

$$
F(x) = 0\mathbbm{1} \{x < 0\} + x\mathbbm{1} \{0 < x < 1\} + \mathbbm{1} \{x \geq 1\}.
$$

Now we reproduce the definition of a Lebesgue-Stieltjes measure on $\mathbb{R}$ from the textbook (p. 25). For $x \in \mathbb{R}$, let $F(x+) \equiv \lim_{y \downarrow x} F(y)$ and $F(x-) \equiv \lim_{y \uparrow x} F(y)$. Set $F(\infty) = \lim_{x \uparrow \infty} F(x)$ and $F(-\infty) = \lim_{x \downarrow -\infty} F(x)$. Let

$$
\mathcal{C} \equiv \left\{(a, b] : -\infty \leq a \leq b < \infty \right\} \cup \left\{(a, \infty) : -\infty \leq a < \infty \right\}
$$

Define

$$
\begin{aligned}
\mu_F((a, b])) &= F(b+) - F(a+)\\[10pt]
\mu_F((a, \infty)) &= F(\infty) - F(a+)
\end{aligned}
$$

Note that

$$
\begin{aligned}
F(\infty) &= \lim_{x \uparrow \infty} F(x) = 1\\[10pt]
F(-\infty) &= \lim_{x \downarrow -\infty} F(x) = 0.
\end{aligned}
$$

## (iii)



## (iv)

&nbsp;&nbsp;&nbsp;&nbsp; Given $\mu_F^*$ is a measure defined on a semialgebra (i.e., $\mathcal{C}$), let $\mu_F^*$ be the outer measure induced by $\mu_F$, defined on $\mathcal{P}(\mathbb{R})$. Then we can define

$$
\mathcal{M} \equiv \mathcal{M}_{\mu_F^*} \equiv \{A : A \text{ is } \mu_F^*\text{-measurable}\}.
$$

Next, by Theorem 1.3.2,

- (i) $\mathcal{M}$ is a $\sigma$-algebra,
- (ii) $\mu_F^*$ restricted to $\mathcal{M}$ is a measure, and
- (iii) $\mu_F^*(A) = 0 \implies \mathcal{P}(A) \subset \mathcal{M}$.

Then, by Caratheodory's extension theorem,

- (i) $\mu_F^*$ is an outer measure,
- (ii) $\mathcal{C} \subset \mathcal{M}_{\mu_F^*}$, and
- (iii) $\mu_F^* = \mu_F$ on $\mathcal{C}$.

Finally, since $\sigma \langle \mathcal{C} \rangle = \mathcal{B}(\mathbb{R})$, the class of all Borel sets of $\mathbb{R}$, $\mu_F^*$ is also a measure on $(\mathbb{R}, \mathcal{B}(\mathbb{R})$ (p.26 in textbook).  

## (v) [Fix]

&nbsp;&nbsp;&nbsp;&nbsp; Yes, this extension is unique. Recall that $\mu^F$ is a $\sigma$-finite measure on the semialgebra $\mathcal{C}$. And note that $\mu_F^*$ is a measure on the measurable space $(\mathbb{R}, \sigma \langle \mathcal{C} \rangle)$ such that $\mu_F = \mu_F^*$. Then $\mu_F^* = \mu_F^*$ on $\sigma \langle \mathcal{C} \rangle$ (Theorem 1.3.6).

